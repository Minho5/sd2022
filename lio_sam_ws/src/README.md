# Camera-LiDAR object detection


## Installation
### Dependencies

This software is built on the Robotic Operating System ([ROS]), which needs to be [installed](http://wiki.ros.org) first. Additionally, YOLO for ROS depends on following software:

- [OpenCV](http://opencv.org/) (computer vision library),
- [boost](http://www.boost.org/) (c++ library),

This software include opensource algorithm from https://github.com/LidarPerception/segmenters_lib and https://github.com/KevinJia1212/colored_pointcloud

- Catkin build in src folder

```bash 
catkin build --save-config --cmake-args -DCMAKE_BUILD_TYPE=Release
```

---

## Execution
- Launch ouster (add ```replay:=true``` for replaying)

```bash 
roslaunch ouster_ros os1.launch
```

- Rosbag

```bash 
rosparam set use_sim_time true
rosbag play --clock [bagfile path]
```

- Launch darknet ros

```bash
roslaunch darknet_ros yolo_v3.launch
```
Change parameters from ~/darknet_ws/src/darknet_ros/config/yolov3.yaml

- Launch pointcloud projection

```bash
roslaunch colored_pointcloud colored_pointcloud_node161.launch
```

- Launch segmentation library

```bash
roslaunch segmenters_lib demo.launch
```
Change parameters from ~/perception/libs/segmenters/config



